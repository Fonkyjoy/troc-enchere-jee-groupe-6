package fr.eni.javaee.trocenchere.view;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.eni.javaee.trocenchere.bll.UtilisateurManager;
import fr.eni.javaee.trocenchere.bo.Utilisateur;
import fr.eni.javaee.trocenchere.exception.BusinessException;

/**
 * Servlet implementation class SuprimProfilServlet
 */
@WebServlet("/SuprimProfilServlet")
public class SuprimProfilServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SuprimProfilServlet() {
        super();
     
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("je suis suprimeServlet L34");
		HttpSession session = request.getSession();
		Utilisateur userInSession = (Utilisateur) session.getAttribute("userInSession");
		UtilisateurManager utilisateurManager = UtilisateurManager.getUtilisateurManager();
		try {
			utilisateurManager.supprimUtilisateur(userInSession.getNoUtilisateur());
			
		} catch (BusinessException e) {
			e.printStackTrace();
			request.setAttribute("errors", e.getErrors());
		}	
		System.out.println("je suis modifServlet L33");
		request.getRequestDispatcher("/WEB-INF/index.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
