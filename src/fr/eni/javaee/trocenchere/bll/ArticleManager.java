/**
 * 
 */
package fr.eni.javaee.trocenchere.bll;
import java.time.LocalDate;
import fr.eni.javaee.trocenchere.bo.Article;
import fr.eni.javaee.trocenchere.dal.ArticleDAO;
import fr.eni.javaee.trocenchere.dal.DAOFactory;
import fr.eni.javaee.trocenchere.exception.BusinessException;

/**
 * @author acueff2020
 *
 */
public class ArticleManager {
	private static ArticleDAO articleDAO;
	// Pattern Singleton
	private static ArticleManager instance;
	// Getter static
	public static ArticleManager getArticleManager() { 
		if(instance== null ) { 
			instance = new ArticleManager();
			} 
		return instance; 
		}
	/**
	 * Constructeur.
	 */
	public ArticleManager() {
		articleDAO = DAOFactory.getArticleDAO();
	}
	public static Article addArticle( String nomArticle, String description,int noCategorie, int miseAPrix , LocalDate dateFinEncheres, LocalDate datedebutEncheres) throws BusinessException {
			BusinessException be = new BusinessException();
			boolean isvalidDesChamps = validerDesChamps(nomArticle,description,noCategorie,miseAPrix,dateFinEncheres,datedebutEncheres,be);

		if (isvalidDesChamps) {
			Article article = new Article();
		
			articleDAO.createArticle(article);
			return(article); 
	}else{throw be;}}
	/**
	 * @param nomArticle
	 * @param description
	 * @param noCategorie
	 * @param miseAPrix
	 * @param dateFinEncheres
	 * @param datedebutEncheres
	 * @param be
	 * @return
	 */
	private static boolean validerDesChamps(String nomArticle, String description, int noCategorie, int miseAPrix,
			LocalDate dateFinEncheres, LocalDate datedebutEncheres, BusinessException be) {
		if( nomArticle == null || nomArticle.trim().isEmpty()
				|| description == null || description.trim().isEmpty() 
				|| noCategorie == 0
				|| miseAPrix == 0  
				|| dateFinEncheres == null 
				|| datedebutEncheres == null )
					 {
				be.addError("Ces données sont obligatoires");
				return false;
			}
			return true;
		
	}
	/**
	 * @param noUtilisateur
	 * @return
	 */
	public Article afficheArticle(int noUtilisateur) throws BusinessException{
		Article article = null;
		article = articleDAO.selectArticle(noUtilisateur);
		return article;
		
	}
			
			
	

}
