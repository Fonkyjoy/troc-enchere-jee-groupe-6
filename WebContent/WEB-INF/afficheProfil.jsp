<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content=" ">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>TrocEnchere</title>

    <!-- Bootstrap -->
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- <link href="${pageContext.servletContext.contextPath}/Vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">-->
</head>

<body style="background-color: rgb(252, 245, 235);">
	<!-- header -->
    <!-- nav -->
    <nav class="navbar navbar-default navbar navbar-inverse ">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand " href="#" style="font-size:20% font-weight:bold ">
                  <h4 style="font-weight:bold"> ENI-Enchères</h4>
                  </a>
            </div>
        </div>
    </nav>
     <div class="container"> 
        <div class="row" >
                <div class="row">
                    <div class="col-xs-4 col-xs-offset-4" style=" height: 100px ;">
                        <h2 style="font-weight:bold">Mon profil</h2> 
                    </div>
                </div>
            <form action="${pageContext.request.contextPath}/AfficheProfilServlet"  method="post">    
               
                <table class="col-sm-8 col-sm-offset-2 table table-bordered table-hover  " style="max-width: 65%;"  >
                    <tr>
                        <th>Pseudo : </th>
                        <td >${utilisateur.pseudo}</td>
                    </tr>
                    <tr>
                        <th>Nom : </th>
                        <td >${utilisateur.nom}</td>
                    </tr>
                    <tr >
                        <th>Prénom : </th>
                        <td >${utilisateur.prenom}</td>
                    </tr>
                    <tr>
                        <th>Email : </th>
                        <td>${utilisateur.email}</td>
                    </tr>
                    <tr>
                        <th>Teléphone : </th>
                        <td ><c:out value="${utilisateur.telephone}" /> </td>
                    </tr>
                    <tr>
                        <th>Rue : </th>
                        <td >${utilisateur.rue}</td>
                    </tr>
                    <tr>
                        <th>Code postal : </th>
                        <td>${utilisateur.codePostal}</td>
                    </tr>
                    <tr>
                        <th>Ville : </th>
                        <td>${utilisateur.ville}</td>
                    </tr>
                </table>
     
                    <div class=" row form-group" >
                        <div class=" col-sm-3 col-sm-offset-4">
							<%--<input type="submit" value="Modifier" class="btn btn-primary btn-lg active"> --%> 	
							<a class="btn btn-primary" href="<%=request.getContextPath()%>/ModifProfilServlet">Modifier</a>
						</div>
                        <div class="col-sm-3 ">
                          <%-- <input type="submit" value="Accueil" class="btn btn-primary btn-lg active"> --%> 	
                           <a class="btn btn-primary" href="<%=request.getContextPath()%>/">Accueil</a> 
                        </div>
                    </div>
               </form>     
			 </div> 	
            </div> 
        <!-- FOOTER -->

        <script src="https://cdn.jsdelivr.net/npm/jquery@1.12.4/dist/jquery.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js"></script>
    <!-- </div> -->
</body>

</html>