package fr.eni.javaee.trocenchere.view;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.eni.javaee.trocenchere.bll.ArticleManager;
import fr.eni.javaee.trocenchere.bo.Article;
import fr.eni.javaee.trocenchere.bo.Utilisateur;
import fr.eni.javaee.trocenchere.exception.BusinessException;

/**
 * Servlet implementation class AfficheProfilServlet
 */
@WebServlet("/AfficheNouvelleVente")
public class AfficheNouvelleVenteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		Utilisateur userInSession = (Utilisateur) session.getAttribute("userInSession");
		Article article= null;
        article.setNoUtilisateur(userInSession.getNoUtilisateur());
		
		int noUtilisateur = 0;
		ArticleManager articleManager = new ArticleManager();
		
		if (session.getAttribute("noUtilisateur") != null) {
			article = (Article) session.getAttribute("noUtilisateur");
			noUtilisateur = article.getNoUtilisateur();
		}
		try {
			
			noUtilisateur = Integer.parseInt(request.getParameter("noUtilisateur"));
			article = articleManager.afficheArticle(noUtilisateur);
			request.setAttribute("article", article);
			
			request.getRequestDispatcher("/WEB-INF/afficheProfil.jsp").forward(request, response);
		} catch (BusinessException e) {
			e.printStackTrace();
			request.setAttribute("errors", e.getErrors());
		}

	}
}

