package fr.eni.javaee.trocenchere.dal;

import fr.eni.javaee.trocenchere.bo.Utilisateur;
import fr.eni.javaee.trocenchere.exception.BusinessException;

public interface UtilisateurDAO {
	public void createUtilisateur(Utilisateur utilisateur)throws BusinessException;
	public boolean pseudoExiste(String pseudo)throws BusinessException;
	public Utilisateur selectUtilisateur(String pseudo)throws BusinessException;
	
	public void updateUtilisateur(Utilisateur utilisateur)throws BusinessException;
	public void deleteUtilisateur(int noUtilisateur)throws BusinessException;
	
	public Utilisateur find(String pseudo, String motDePasse) throws BusinessException;
	
	
}
