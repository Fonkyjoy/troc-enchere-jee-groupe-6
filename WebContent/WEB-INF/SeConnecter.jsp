<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="java.util.List"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!-- Obliger la JSP à rediriger les erreurs sur error.jsp -->
<%@ page errorPage="error.java" isErrorPage="false"%>
<!--  Equivaut à TRY CATCH -->

<!DOCTYPE html>
<html>

<head>

<meta charset="utf-8">

<title>SeConnecterJSP</title>

<link href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css" rel="stylesheet">
<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

</head>

<body style="background-color: rgb(252, 245, 235);">
	<nav class="navbar navbar-default navbar navbar-inverse ">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand " href="#" style="font-size:20% font-weight:bold ">
                  <h4 style="font-weight:bold"> ENI-Enchères</h4>
                  </a>
            </div>
        </div>
    </nav>

	<div class="container">

		<!-- Page Heading -->
		

		<%
			if (request.getAttribute("errors") != null) {
			List<String> errors = (List<String>) request.getAttribute("errors");
		%>
		<div class="col-lg-3 col-md-4 col-sm-6 portfolio-item">
			<div class="card h-100 alert">
				<div class="card-body alert alert-danger">
					<h4 class="card-title">
						<h2>Erreurs</h2>
						<%
							for (String msg : errors) {
						%>
						<p class="card-text"><%=msg%></p>
						<%
							}
						%>
					</h4>
				</div>
			</div>
		</div>
		<%
			}
		%>
		<div class="row">
			<div class="col-lg-4 col-md-4 col-sm-4 portfolio-item">
				<div class="card h-100">
					<div class="card-body">
						<form method="post" action="./SeConnecter">
							<div class="form-group">
								<label for="pseudo">Identifiant : </label>
								 <input class="form-control" id="pseudo" required name="pseudo" value="${pseudo}">
							</div>
							<div class="form-group">
								<label for="motDePasse">Mot de passe : </label> 
								<input type="password" class="form-control" id="motDePasse" required name="motDePasse" value="${motDePasse}">
							</div>
							<div class="form-group">
								<input type="submit" value="Connexion" class="btn btn-primary active">
	
								<input type="checkbox" id="rememberme" name="rememberme" value="1" checked> 
								<label for="rememberme" >Se souvenir de moi</label> 
								<p><a href="MotDePasseOublie">Mot de passe oublié</a></p>
							</div>
							
						</form>

						
						<a class="btn btn-primary active" href="<%=request.getContextPath()%>/CreerCompte"> Créer un compte </a>
					</div>
				</div>
			</div>
		</div>

		<!-- /.row -->

	</div>
	<!-- /.container -->



	<!-- Bootstrap core JavaScript -->
	<script src="vendor/jquery/jquery.min.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

</body>

</html>
