/**
 * 
 */
package fr.eni.javaee.trocenchere.dal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import fr.eni.javaee.trocenchere.bo.Utilisateur;
import fr.eni.javaee.trocenchere.exception.BusinessException;

/**
 * @author jlemauff2021
 *
 */
public class UtilisateurDAOjdbclmpl implements UtilisateurDAO {
	private static final String INSERT_PROFIL = "insert into UTILISATEURS(pseudo,nom,prenom,"
			+ " email,telephone,rue,code_postal,ville,mot_de_passe,credit,administrateur) values(?,?,?,?,?,?,?,?,?,?,?)";
	
	private static final String SELECT_PSEUDO = "select pseudo from UTILISATEURS WHERE pseudo = ?";
	private static final String SELECT_PROFIL = "select no_utilisateur, pseudo, nom, prenom, email, telephone, rue, code_postal, ville,mot_de_passe,credit,administrateur from UTILISATEURS WHERE pseudo = ?";
	private static final String UPDATE_PROFIL = "update UTILISATEURS set pseudo=?,nom=?,prenom=?,email=?,telephone=?,rue=?,code_postal=?,ville=?,mot_de_passe=? WHERE no_utilisateur =?";
	private static final String DELETE_PROFIL = "delete from UTILISATEURS WHERE no_utilisateur = ?";
	
	private static final String UTILISATEUR_CONNECTE = "SELECT pseudo, mot_de_passe FROM UTILISATEURS WHERE pseudo=? and mot_de_passe=?";
	
	private Utilisateur utilisateurBuilder(ResultSet rs) throws SQLException {
		Utilisateur utilisateur = new Utilisateur();
		utilisateur.setNoUtilisateur(rs.getInt("no_utilisateur"));
		utilisateur.setPseudo(rs.getString("pseudo"));
		utilisateur.setNom(rs.getString("nom"));
		utilisateur.setPrenom(rs.getString("prenom"));
		utilisateur.setEmail(rs.getString("email"));
		utilisateur.setTelephone(rs.getString("telephone"));
		utilisateur.setRue(rs.getString("rue"));
		utilisateur.setCodePostal(rs.getString("code_postal"));
		utilisateur.setVille(rs.getString("ville"));
		utilisateur.setMotDePasse(rs.getString("mot_de_passe"));
		utilisateur.setCredit(rs.getInt("credit"));
		utilisateur.setAdministrateur(rs.getShort("administrateur"));
		return utilisateur;	
	}
	
	@Override
	public void createUtilisateur(Utilisateur utilisateur)throws BusinessException{
		try(Connection cnx = ConnectionProvider.getConnection()){
				PreparedStatement insert = cnx.prepareStatement(INSERT_PROFIL,PreparedStatement.RETURN_GENERATED_KEYS);
				insert.setString(1,utilisateur.getPseudo());
				insert.setString(2,utilisateur.getNom());
				insert.setString(3,utilisateur.getPrenom());
				insert.setString(4,utilisateur.getEmail());
				insert.setString(5,utilisateur.getTelephone());
				insert.setString(6,utilisateur.getRue());
				insert.setString(7,utilisateur.getCodePostal());
				insert.setString(8,utilisateur.getVille());
				insert.setString(9,utilisateur.getMotDePasse());
				insert.setInt(10,utilisateur.getCredit());
				insert.setShort(11,utilisateur.getAdministrateur());
				insert.executeUpdate();
				ResultSet rs = insert.getGeneratedKeys(); 
				if(rs.next()) {
					utilisateur.setNoUtilisateur(rs.getInt(1));
				}else {
					BusinessException be = new BusinessException();
					be.addError("Impossible de créer un compte ");
					throw be;
				}
				rs.close();		
		}catch (SQLException e) {
			e.printStackTrace();
			BusinessException be = new BusinessException();
			be.addError("Impossible de créer un compte ");
			throw be;
		}
	}
	public boolean pseudoExiste(String pseudo )throws BusinessException{
		try(Connection cnx = ConnectionProvider.getConnection()){
			PreparedStatement pstmt = cnx.prepareStatement(SELECT_PSEUDO);
			pstmt.setString(1,pseudo);
			ResultSet rs = pstmt.executeQuery();
			if(rs.next()) {
				return true;
			}
				return false;	
		} catch (SQLException e) {
			e.printStackTrace();
			BusinessException be = new BusinessException();
			be.addError("Impossible de trouver pseudo ");
			throw be;
		}
	}
	@Override
	public Utilisateur selectUtilisateur(String pseudo)throws BusinessException{
		Utilisateur utilisateur = new Utilisateur();
		try(Connection cnx = ConnectionProvider.getConnection()) {
			PreparedStatement pstmt = cnx.prepareStatement(SELECT_PROFIL);
			pstmt.setString(1, pseudo);
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				utilisateur = utilisateurBuilder(rs);				
			}
		} catch (SQLException e) {
			e.printStackTrace();
			BusinessException be = new BusinessException();
			be.addError("Impossible de trouver pseudo ");
			throw be;
		}
		return utilisateur;
		
	}
	
	/**
	* {@inheritDoc}
	*/
	@Override
	public void updateUtilisateur(Utilisateur utilisateur) throws BusinessException {
		try(Connection cnx = ConnectionProvider.getConnection()) {
			PreparedStatement pstmt = cnx.prepareStatement(UPDATE_PROFIL);
			pstmt.setString(1, utilisateur.getPseudo());
			pstmt.setString(2,utilisateur.getNom());
			pstmt.setString(3,utilisateur.getPrenom());
			pstmt.setString(4,utilisateur.getEmail());
			pstmt.setString(5,utilisateur.getTelephone());
			pstmt.setString(6,utilisateur.getRue());
			pstmt.setString(7,utilisateur.getCodePostal());
			pstmt.setString(8,utilisateur.getVille());
			pstmt.setString(9,utilisateur.getMotDePasse());
			pstmt.setInt(10, utilisateur.getNoUtilisateur());
			
			pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			BusinessException be = new BusinessException();
			be.addError("Impossible de modifier ce profil ");
			throw be;
		}
		
	}
	/**
	* {@inheritDoc}
	*/
	@Override
	public void deleteUtilisateur(int noUtilisateur) throws BusinessException {
		try(Connection cnx = ConnectionProvider.getConnection()) {
			PreparedStatement pstmt = cnx.prepareStatement(DELETE_PROFIL);
			pstmt.setInt(1, noUtilisateur);
			pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			BusinessException be = new BusinessException();
			be.addError("Impossible de suprimer ce compte ");
			throw be;
		}
		
	}
	/**
	* {@inheritDoc}
	*/
	@Override
	public Utilisateur find(String pseudo, String motDePasse) throws BusinessException {
		try(Connection cnx = ConnectionProvider.getConnection()){
			PreparedStatement pstmt = cnx.prepareStatement(UTILISATEUR_CONNECTE);
			pstmt.setString(1, pseudo);
			pstmt.setString(2, motDePasse);
			
			ResultSet rs = pstmt.executeQuery();
			if(rs.next()) {
				Utilisateur u = new Utilisateur();
				u.setPseudo(rs.getString("pseudo"));
				u.setMotDePasse(rs.getString("mot_de_passe"));
				return u;}
			else {
				BusinessException be = new BusinessException();
				be.addError("Identifiant ou Mot de passe inconnu");
				throw be;
		}			
		} catch (SQLException e) {
			e.printStackTrace();
			BusinessException be = new BusinessException();
			be.addError("Error DB");
			throw be;			
		}	
	}

	
}
