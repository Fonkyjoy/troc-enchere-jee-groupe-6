/**
 * 
 */
package fr.eni.javaee.trocenchere.bll;
import fr.eni.javaee.trocenchere.bo.Utilisateur;
import fr.eni.javaee.trocenchere.dal.DAOFactory;
import fr.eni.javaee.trocenchere.dal.UtilisateurDAO;
import fr.eni.javaee.trocenchere.exception.BusinessException;
import fr.eni.javaee.trocenchere.utils.Constants;

/**
 * Classe en charge
 * @author jlemauff2021
 * @version TrocEnchere - v1.0
 * @date 20 janv. 2021 - 23:21:17
 */
public class UtilisateurManager {
	private UtilisateurDAO utilisateurDAO;
	// Pattern Singleton
	private static UtilisateurManager instance;
	// Getter static
	public static UtilisateurManager getUtilisateurManager() { 
		if(instance== null ) { 
			instance = new UtilisateurManager();
			} 
		return instance; 
		}
	 
	/**
	 * Constructeur.
	 */
	public UtilisateurManager() {
		utilisateurDAO = DAOFactory.getUtilisateurDAO();
	}
	
	
	public void supprimUtilisateur(int noUtilisateur)throws BusinessException{
		utilisateurDAO.deleteUtilisateur(noUtilisateur);
	}
	
	public void modifUtilisateur(Utilisateur utilisateur)throws BusinessException{
		utilisateurDAO.updateUtilisateur(utilisateur);
	}

	public  Utilisateur afficheUtilisateur(String pseudo)throws BusinessException{
		Utilisateur user = null;
		user = utilisateurDAO.selectUtilisateur(pseudo);
		return user;	
	}
	public Utilisateur addUtilisateur( String pseudo, String nom, String prenom, String email, String telephone, String rue,
		String codePostal, String ville, String motDePasse,String motDePassConfir) throws BusinessException {
		
		BusinessException be = new BusinessException();
		boolean isvalidPseudo = validerPseudo(pseudo,be); 
		boolean isvalidDesChamps = validerDesChamps(pseudo,nom,prenom,email,telephone,rue,codePostal,ville,motDePasse,motDePassConfir,be);
		boolean isvalidMotDePasse = validerMotDePasse(motDePasse,be);
		boolean isvalidMotDePasseConfir = validerMotDePasseConfir(motDePasse,motDePassConfir,be);
		
		if(isvalidDesChamps && isvalidPseudo && isvalidMotDePasse && isvalidMotDePasseConfir ) {
		Utilisateur	utilisateur = new Utilisateur();
		utilisateur.setPseudo(pseudo);
		utilisateur.setNom(nom);
		utilisateur.setPrenom(prenom);
		utilisateur.setEmail(email);
		utilisateur.setTelephone(telephone);
		utilisateur.setRue(rue);
		utilisateur.setCodePostal(codePostal);
		utilisateur.setVille(ville);
		utilisateur.setMotDePasse(motDePasse);
		utilisateur.setMotDePasse(motDePassConfir);
		utilisateurDAO.createUtilisateur(utilisateur);
		return(utilisateur); 
		}else {
			throw be;
		}
	}
	

	/**
	 * Méthode en charge de
	 * @param motDePasse
	 * @param be
	 * @return
	 */
	private boolean validerMotDePasse(String motDePasse, BusinessException be) {
		if(!motDePasse.matches(Constants.PATTERN_PWD)) {
			be.addError("Mot de passe doit contenir entre 8 et 12 caractères (1 chiffre, 1 majuscule, 1 caractère spéciale)");
			return false;
		}
		return true;
	}

	/**
	 * Méthode en charge de
	 * @param motDePassConfir
	 * @param be
	 * @return
	 */
	private boolean validerMotDePasseConfir(String motDePasse,String motDePassConfir, BusinessException be) {
		if(motDePassConfir == motDePasse ) {
			be.addError("mot de passe est non validé");
			return false;
		}
		return true;
	}

	/**
	 * Méthode en charge de
	 * @param nom
	 * @param prenom
	 * @param email
	 * @param telephone
	 * @param rue
	 * @param codePostal
	 * @param ville
	 * @param be
	 * @return
	 */
	private boolean validerDesChamps(String pseudo,String nom, String prenom, String email, String telephone, String rue,
			String codePostal, String ville,String motDePasse,String motDePassConfir, BusinessException be) {
		if( pseudo == null || pseudo.trim().isEmpty()
			|| nom == null || nom.trim().isEmpty() 
			|| prenom == null || prenom.trim().isEmpty()
			|| email == null || email.trim().isEmpty() 
			|| telephone == null || telephone.trim().isEmpty()
			|| rue == null || rue.trim().isEmpty()
			|| codePostal == null || codePostal.trim().isEmpty() 
			|| ville == null || ville.trim().isEmpty()
			|| motDePasse == null || motDePasse.trim().isEmpty() 
			|| motDePassConfir == null || motDePassConfir.trim().isEmpty()
			
				) {
			be.addError("ici est obligatoire");
			return false;
		}
		return true;
	}

	/**
	 * Méthode en charge de
	 * @param pseudo
	 * @param be
	 * @throws BusinessException 
	 */
	private boolean validerPseudo(String pseudo, BusinessException be) throws BusinessException {
		
		if(!pseudo.matches(Constants.PATTERN_PSEUDO)) {
			be.addError("pseudo doit contenir 10 et 30 caractères alphanumériques");
			return false;
		}else if(utilisateurDAO.pseudoExiste(pseudo)) {
			be.addError("pseudo est déjà utilisé ");
			return false;
		}else if(pseudo.trim().length() > 30 || pseudo.trim().length() < 10 ) {
			be.addError("pseudo doit contenir entre 10 et 30 caractères");
			return false;
		}
		return true;
	}
	
	public Utilisateur validateConnexion(String pseudo, String motDePasse) throws BusinessException {
		BusinessException be = new BusinessException();
		boolean isValidPseudo = validatePseudo(pseudo, be);
		boolean isValidMotDePasse = validateMotDePasse(motDePasse, be);
		if (isValidPseudo && isValidMotDePasse) {
			return utilisateurDAO.find(pseudo, motDePasse);
		} else {
			throw be;
		}
	}
	
	private boolean validatePseudo(String pseudo, BusinessException be) {
		if (pseudo == null || pseudo.isEmpty() ) {
			be.addError("Pseudo est obligatoire");
			return false;
		}
		if (!pseudo.matches(Constants.PATTERN_PSEUDO)) {
			be.addError("pseudo doit contenir 10 et 30 caractères alphanumériques");
			return false;
		}
		return true;
	}

	private boolean validateMotDePasse(String motDePasse, BusinessException be) {
		if (motDePasse == null) {
			be.addError("Mot de passe est obligatoire");
			return false;
		}
		if (!motDePasse.matches(Constants.PATTERN_PWD)) {
			be.addError(
					"Mot de passe doit contenir entre 8 et 12 caractères (1 chiffre, 1 majuscule, 1 caractère spéciale)");
			return false;
		}
		return true;
	
}
}
