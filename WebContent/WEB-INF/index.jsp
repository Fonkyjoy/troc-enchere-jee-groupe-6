<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Navigation</title>

<link href="Vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css" rel="stylesheet">


</head>
<body>
	<main class="container">

		<header class="row">
			<!-- header col1 -->
			<div class="col-lg-9 col-md-6 col-sm-12 col-12 ">
				<h1 class="my-4">ENI-Encheres</h1>
			</div>
			<!-- header col2 -->
			<div class="col-lg-3 col-md-6 col-sm-12 col-12 ">
				<a href="${pageContext.request.contextPath}/CreerCompte">S'inscrire</a>
				<a href="${pageContext.request.contextPath}/SeConnecter">Se connecter</a>
			</div>

		</header>


		<!-- Page raw2 -->
		<section class="row">
			<!-- section Filtres -->
			<div class="col-lg-6 col-md-6 col-sm-12 col-12 ">
				<h3>Filtres</h3>
				<nav class="navbar navbar-light bg-light">
					<form class="form-inline">
						<input class="form-control mr-sm-2" type="search"
							placeholder="Le nom de l'article contient" aria-label="Search">
						<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Rechercher</button>
					</form>
				</nav>

				<aside class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-12">
						<label for="lstcate">Catégorie : </label> <select id="lstcate">
							<option selected value="SELECT">Toutes</option>
							<c:forEach items="${lstCategories}" var="c">
								<option value="${c.id}">${c.libelle}</option>
							</c:forEach>
						</select>
					</div>
				</aside>
			</div>

		</section>


		<!-- Page raw3 -->
		<section class="row">
			<!-- section Filtres -->
			<div class="btn-group col-lg-6 col-md-6 col-sm-12 col-12">
				<div class="card mb-3" style="max-width: 700px;">
					<div class="row g-0">
						<div class="col-lg-5 col-md-5 col-sm-5 col-5">
							<img src="./images/pc.jpg" class="img-thumbnail"
								alt="image de l'article a vendre">
						</div>
						<div class="col-lg-7 col-md-7 col-sm-7 col-7">
							<div class="card-body">
								<h5 class="card-title">PC Gamer pour Travailler</h5>
								<p class="card-text">prix:</p>
								<p class="card-text">Fin de l'enchère:</p>
								<p class="card-text">Vendeur:</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- fin conteneur -->
	</main>
	<footer>
		<%@include file="template/footer.html"%>
	</footer>
</body>
</html>