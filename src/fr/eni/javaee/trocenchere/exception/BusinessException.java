/**
 * 
 */
package fr.eni.javaee.trocenchere.exception;

import java.util.ArrayList;
import java.util.List;



/**
 * Classe en charge de gérer les exceptions
 *
 */
public class BusinessException extends Exception {

	private static final long serialVersionUID = -1276418438727651144L;

	private List<String> errors;

	

	public void addError(String error) {
		if(errors == null) {
			errors = new ArrayList<String>();
		}
		errors.add(error);
	}
	
	public List<String> getErrors() {
		return errors;
	}
	
	
	

}
