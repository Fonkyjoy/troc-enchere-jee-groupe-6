/**
 * 
 */
package fr.eni.javaee.trocenchere.dal;

import java.util.List;

import fr.eni.javaee.trocenchere.bo.Categorie;
import fr.eni.javaee.trocenchere.exception.BusinessException;

/**
 * @author dfrage2020
 *
 */
public interface CategorieDAO {
		// CRUD
		public void create (Categorie categorie) throws BusinessException;
		public List<Categorie> findAllCategorie() throws BusinessException;
	
	}
