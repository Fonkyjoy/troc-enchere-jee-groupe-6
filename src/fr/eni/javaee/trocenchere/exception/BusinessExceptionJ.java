/**
 * 
 */
package fr.eni.javaee.trocenchere.exception;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe en charge
 * @author jlemauff2021
 * @version TrocEnchere - v1.0
 * @date 20 janv. 2021 - 22:29:31
 * 
 * Cette classe permet de recenser l'ensemble des erreurs (par leur code) 
 * pouvant survenir lors d'un traitement quel que soit la couche à l'origine.
 */
public class BusinessException extends Exception{
	private static final long serialVersionUID = 1L;
	private List<String> errors = new ArrayList<String>();
	public void addError(String error) {
		errors.add(error);
	}
	public List<String> getErrors() {
		return errors;
	}
	public boolean hasErreurs() {
		return this.errors.size() > 0;
	}
}
