/**
 * 
 */
package fr.eni.javaee.trocenchere.bo;

/**
 * @author acueff2020
 *
 */
public class Retrait {
private String rue;
private String code_postal;
private String ville;
private Article articleVendu;



public Retrait() {
	super();
}
public Retrait(String rue, String code_postal, String ville) {
	super();
	this.rue = rue;
	this.code_postal = code_postal;
	this.ville = ville;}
	
public Retrait(String rue, String code_postal, String ville, Article articleVendu) {
	super();
	this.articleVendu = articleVendu;
}



public String getRue() {
	return rue;
}
public void setRue(String rue) {
	this.rue = rue;
}
public String getCode_postal() {
	return code_postal;
}
public void setCode_postal(String code_postal) {
	this.code_postal = code_postal;
}
public String getVille() {
	return ville;
}
public void setVille(String ville) {
	this.ville = ville;
}
public Article getArticleVendu() {
	return articleVendu;
}
public void setArticleVendu(Article articleVendu) {
	this.articleVendu = articleVendu;
}
@Override
public String toString() {
	return "Retrait [rue=" + rue + ", code_postal=" + code_postal + ", ville=" + ville + ", articleVendu="
			+ articleVendu + "]";
}
@Override
public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((articleVendu == null) ? 0 : articleVendu.hashCode());
	return result;
}
@Override
public boolean equals(Object obj) {
	if (this == obj)
		return true;
	if (obj == null)
		return false;
	if (getClass() != obj.getClass())
		return false;
	Retrait other = (Retrait) obj;
	if (articleVendu == null) {
		if (other.articleVendu != null)
			return false;
	} else if (!articleVendu.equals(other.articleVendu))
		return false;
	return true;
}




}
