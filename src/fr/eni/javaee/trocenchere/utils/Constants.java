/**
 * 
 */
package fr.eni.javaee.trocenchere.utils;

/**
 * @author acueff2020
 *
 */
public interface Constants {
	String PATTERN_PWD = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,12}";
	
	String PATTERN_PSEUDO = "^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{10,30}$";
}


