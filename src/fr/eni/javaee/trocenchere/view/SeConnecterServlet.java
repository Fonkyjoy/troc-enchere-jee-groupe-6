package fr.eni.javaee.trocenchere.view;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.eni.javaee.trocenchere.bll.CategorieManager;
import fr.eni.javaee.trocenchere.bll.UtilisateurManager;
import fr.eni.javaee.trocenchere.bo.Categorie;
import fr.eni.javaee.trocenchere.bo.Utilisateur;
import fr.eni.javaee.trocenchere.exception.BusinessException;



/**
 * Servlet implementation class SeConnecterServlet
 */
@WebServlet("/SeConnecter")
public class SeConnecterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("/WEB-INF/SeConnecter.jsp").forward(request, response);
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session =  request.getSession();
		Cookie[] cookies = request.getCookies();
		String pseudo = request.getParameter("pseudo");
		String motDePasse = request.getParameter("motDePasse");
		String remMotDePasse = request.getParameter("rememberme");
		try {
			//Appelle BLL
			Utilisateur user = UtilisateurManager.getUtilisateurManager().validateConnexion(pseudo, motDePasse);
			List<Categorie> listeCategories = CategorieManager.getCategorieManager().allCategorie();
			request.setAttribute("lstCategories", listeCategories);
			if(user.getPseudo().equals(pseudo) && user.getMotDePasse().equals(motDePasse)) {
				if(remMotDePasse != null && ! remMotDePasse.isEmpty()) {
			
					Cookie cookiePseudo = new Cookie("pseudo", pseudo);
					cookiePseudo.setMaxAge(60*60);
					Cookie cookieMotDePasse = new Cookie("motDePasse", motDePasse);
					cookieMotDePasse.setMaxAge(60*60);
					response.addCookie(cookiePseudo);
					response.addCookie(cookieMotDePasse);
				}else {
					Cookie cookiePseudo = new Cookie("pseudo", "");
					Cookie cookieMotDePasse = new Cookie("motDePasse","");
					cookiePseudo.setMaxAge(-1);
					cookieMotDePasse.setMaxAge(-1);
					response.addCookie(cookiePseudo);
					response.addCookie(cookieMotDePasse);
				}
				session.setMaxInactiveInterval(60*60); 
				session.setAttribute("userInSession", user);
				
				request.getRequestDispatcher("./encheres").forward(request, response);	
			}
		} catch (BusinessException e) {
			e.printStackTrace();
			request.setAttribute("errors", e.getErrors());
			request.getRequestDispatcher("/WEB-INF/SeConnecter.jsp").forward(request, response);
		}
	}
}