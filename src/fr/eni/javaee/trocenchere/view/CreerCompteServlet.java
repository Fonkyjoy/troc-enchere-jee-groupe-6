package fr.eni.javaee.trocenchere.view;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.eni.javaee.trocenchere.bll.UtilisateurManager;
import fr.eni.javaee.trocenchere.bo.Utilisateur;
import fr.eni.javaee.trocenchere.exception.BusinessException;


/**
 * Servlet implementation class CreerCompteServlet
 */
@WebServlet("/CreerCompte")
public class CreerCompteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("/WEB-INF/creerCompte.jsp").forward(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		UtilisateurManager utilisateurManager = new UtilisateurManager();
		request.setCharacterEncoding("UTF-8");
		String pseudo  = request.getParameter("pseudo");
		String nom = request.getParameter("nom");
		String prenom = request.getParameter("prenom");
		String email  = request.getParameter("email");
		String telephone = request.getParameter("telephone");
		String rue = request.getParameter("rue");
		String codePostal = request.getParameter("postcode");
		String ville  = request.getParameter("ville");
		String motDePasse  = request.getParameter("password");
		String motDePassConfir = request.getParameter("repassword");
		//Appelle BLL
		try {
			Utilisateur utilisateur = utilisateurManager.addUtilisateur(pseudo, nom, prenom, email, telephone, rue, codePostal, ville, motDePasse, motDePassConfir);
			HttpSession session =  request.getSession();
			session.setMaxInactiveInterval(60*60);
			session.setAttribute("userInSession", utilisateur);
			request.getRequestDispatcher("/WEB-INF/afficheProfil.jsp").forward(request, response);
		} catch (BusinessException e) {
			e.printStackTrace();
			request.setAttribute("errors", e.getErrors());
			request.getRequestDispatcher("/WEB-INF/CreerCompte.jsp").forward(request, response);
			System.out.println("il y a une erreur");
		}
		
		
	}

}
