
package fr.eni.javaee.trocenchere.dal;
/**
 * Classe en charge
 * @author jlemauff2021
 * @version TrocEnchere - v1.0
 * @date 20 janv. 2021 - 23:13:41
 */
public abstract class DAOFactory {
	
	public static UtilisateurDAO getUtilisateurDAO() {
		return new UtilisateurDAOjdbclmpl() ;
		
	}

	public static ArticleDAO getArticleDAO() {
		return new ArticleJDBCImpl();}
	
	public static CategorieDAO getCategorieDAO() {
		return new CategorieJDBCImpl();
	}
	
	
	
	
}
