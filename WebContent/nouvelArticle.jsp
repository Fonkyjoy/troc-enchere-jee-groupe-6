<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@page import="java.util.List"%>
<!-- Obliger la JSP à rediriger les erreurs sur error.jsp -->
<%@ page errorPage="error.java" isErrorPage="false"%>
<!--  Equivaut à TRY CATCH -->

<!DOCTYPE html>
<html>

<head>

<meta charset="utf-8">

<title>Nouvelle Vente JSP</title>


<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css" rel="stylesheet">

</head>

<body>


	<div class="container">

		<!-- Page Heading -->
		<h1 class="my-4">ENI-Enchères</h1>

		<%
			if (request.getAttribute("errors") != null) {
			List<String> errors = (List<String>) request.getAttribute("errors");
		%>
		<div class="col-lg-3 col-md-4 col-sm-6 portfolio-item">
			<div class="card h-100 alert">
				<div class="card-body alert alert-danger">
					<h4 class="card-title">
						<h2>Erreurs</h2>
						<%
							for (String msg : errors) {
						%>
						<p class="card-text"><%=msg%></p>
						<%
							}
						%>
					</h4>
				</div>
			</div>
		</div>
		<%
			}
		%>
		<div class="row">
			<div class="col-lg-4 col-md-4 col-sm-4 portfolio-item">
				<div class="card h-100">
					<div class="card-body">
						<form method="post"
							action="${pageContext.request.contextPath}/NouvelleVente">
							<div class="form-group">

								<label for="nomArticle">Article: </label> <input type="text"
									name="nomArticle" class="form-control" id="nomArticle">
							</div>
							<div class="form-group">
								<label for="description">Description: </label> <input
									type="text" class="form-control" name="description"
									id="description">
							</div>
							<aside>
							<div class="form-group">
								<label for="lstcate">Catégorie : </label> <select id="lstcate" name="noCategorie">
							<option selected value="SELECT">Toutes</option>
							<c:forEach items="${lstCategories}" var="c">
								<option value="${c.id}">${c.libelle}</option>
							</c:forEach>
								</select>
							</div></aside>
							<div class=form-group>
								<label for="photoArticle">Photo de l'article: </label>
								<button class="btn btn-primary" type="button">Uploader</button>
							</div>
							<div class=form-group>
								<label for="miseAPrix">Mise à prix: </label> <input type=number
									name="miseAPrix" min=0 max=1000 step=10 placeholder="0">
							</div>


							<div class="form-group">

								<label for="datedebutEnchere">Début de l'enchère: </label> <input
									class="form-control" value="${article.dateDebutEnchere}"
									type="date" max="2021-06-15" min="2021-01-23"
									name="datedebutEnchere">

							</div>
							<div class="form-group">
								<label for="dateFinEnchere">Fin de l'enchère: </label> <input
									type="date" max="2021-06-15" min="2021-01-23"
									value="${article.dateFinEnchere}" name="dateFinEnchere">
							</div>


							<div class="text-area">
								<div class="p-3 mb-2 bg-light text-dark">
									
									<div class="form-group">
									<fieldset>
									<legend>Retrait</legend>
										<label for="Rue">Rue: </label> <input type="text"
											class="form-control" id="rue">
										<div class="form-group">
											<label for="CodePostal">Code postal: </label> <input
												type="text" class="form-control" id="code_postal">
										</div>
										<div class="form-group">
											<label for="Ville">Ville: </label> <input name="ville"
												type="text" class="form-control" id="ville">
										</div></fieldset>
									</div>
								</div>
								
								<input type="submit" value="Enregistrer" class="btn btn-primary">
		<input type="reset" value="Annuler" class="btn btn-primary">
							</div></form>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<!-- /.row -->



	<!-- /.container -->



	<!-- Bootstrap core JavaScript -->
	<script src="vendor/jquery/jquery.min.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

</body>

</html>
