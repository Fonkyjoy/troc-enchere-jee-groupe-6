/**
 * 
 */
package fr.eni.javaee.trocenchere.dal;

import fr.eni.javaee.trocenchere.bo.Article;
import fr.eni.javaee.trocenchere.exception.BusinessException;

/**
 * @author acueff2020
 *
 */
public interface ArticleDAO {

	/**
	 * méthode en charge de créer un article
	 */
	public void createArticle(Article article)throws BusinessException; 
		
	public Article selectArticle(int noUtilisateur)throws BusinessException;


}
