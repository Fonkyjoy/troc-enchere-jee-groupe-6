/**
 * 
 */
package fr.eni.javaee.trocenchere.bo;




/**ce
 * @author dfrage2020
 *
 */
public class Categorie {
	private int id;
	private String libelle;

	
	/**
	 * Constructeur par defaut
	 */
	public Categorie() {
		super();
	}
	

	public Categorie(String libelle) {
		super();
		this.libelle = libelle;
	}

	/**
	 * Constructeur avec 
	 * @param noCategories
	 * @param libelle
	 */
	public Categorie(int id, String libelle) {
		super();
		this.id = id;
		this.libelle = libelle;
	}


	


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getLibelle() {
		return libelle;
	}


	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}





	


	


	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(libelle);
		return builder.toString();
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Categorie other = (Categorie) obj;
		if (id != other.id)
			return false;
		return true;
	}



	
	
}

