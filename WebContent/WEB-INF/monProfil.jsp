<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html>
<html lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content=" ">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>TrocEnchere</title>

    <!-- Bootstrap -->
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- <link href="${pageContext.servletContext.contextPath}/Vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">-->
</head>

<body style="background-color: rgb(252, 245, 235);">

    <!-- nav -->
    <nav class="navbar navbar-default navbar navbar-inverse ">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand " href="#">
                <h1>ENI-Enchères</h1>
                 </a>
            </div>
        </div>
    </nav>
     <div class="container"> 
        <div class="row" >
            <div class="col-sm-12 ">
                <div class="row">
                    <div class="col-xs-4 col-xs-offset-4" style=" height: 100px ;">
                        <h3>Mon profil</h3> 
                    </div>
                </div>
                <form class="form-horizontal" method="post" action="<%=request.getContextPath()%>/CreerCompteServlet">
                    <!-- rang 1 -->
                    <div class="form-group col-sm-6 ">
                        <label for="pseudo" class="col-sm-6 control-label alignL" >Pseudo : </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control " id="pseudo" placeholder="">
                        </div>
                    </div>
                    <div class="form-group col-sm-6 ">
                        <label for="nom" class="col-sm-6 control-label alignL" >Nom : </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="nom" placeholder="">
                        </div>
                    </div>

                    <!-- rang 2 -->
                    <div class="form-group col-sm-6 ">
                        <label for="prenom" class="col-sm-6 control-label alignL" >Prénom: </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="prenom" placeholder="">
                        </div>
                    </div>
                    <div class="form-group col-sm-6 ">
                        <label for="email" class="col-sm-6 control-label ">Email : </label>
                        <div class="col-sm-6">
                            <input type="email" class="form-control" id="email" placeholder="">
                        </div>
                    </div>
                    <!-- rang 3 -->
                    <div class="form-group col-sm-6 ">
                        <label for="phone" class="col-sm-6 control-label alignL">Teléphone : </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="phone" placeholder=" ">
                        </div>
                    </div>
                    <div class="form-group col-sm-6 ">
                        <label for="rue" class="col-sm-6 control-label alignL">Rue : </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="rue" placeholder=" ">
                        </div>
                    </div>
                    <!-- rang 4 -->
                    <div class="form-group col-sm-6 ">
                        <label for="postcode" class="col-sm-6 control-label alignL">Code postal : </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="postcode" placeholder="">
                        </div>
                    </div>
                    <div class="form-group col-sm-6 ">
                        <label for="ville" class="col-sm-6 control-label alignL">Ville : </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="ville" placeholder="">
                        </div>
                    </div>
                    <!-- rang 5 -->
                    <div class="form-group col-sm-6 ">
                        <label for="password" class="col-sm-6 control-label alignL">Mot de passe : </label>
                        <div class="col-sm-6">
                            <input type="password" class="form-control" id="password" placeholder="">
                        </div>
                    </div>
                    <div class="form-group col-sm-6 ">
                        <label for="repassword" class="col-sm-6 control-label alignL">Confirmation : </label>
                        <div class="col-sm-6">
                            <input type="password" class="form-control" id="repassword" placeholder="">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-2">
                            <button type="button" class="btn btn-primary btn-lg active">Créer</button>
                             <a
								href="#CreerCompteServlet" class="badge text-success" title="Ajouter"></a>
                        </div>
                        <div class="col-sm-4 col-sm-offset-2 ">
                            <button type="button" class="btn btn-primary btn-lg active">Annuler</button>
                        </div>
                    </div>

                </form>
			 </div> 

            </div> 
        </div>

        <!-- FOOTER -->

        <script src="https://cdn.jsdelivr.net/npm/jquery@1.12.4/dist/jquery.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js"></script>
    <!-- </div> -->
</body>

</html>