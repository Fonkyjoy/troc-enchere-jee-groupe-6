package fr.eni.javaee.trocenchere.view;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.eni.javaee.trocenchere.bll.ArticleManager;
import fr.eni.javaee.trocenchere.bll.CategorieManager;
import fr.eni.javaee.trocenchere.bll.UtilisateurManager;
import fr.eni.javaee.trocenchere.bo.Categorie;
import fr.eni.javaee.trocenchere.bo.Utilisateur;
import fr.eni.javaee.trocenchere.exception.BusinessException;

/**
 * Servlet implementation class NouvelleVente
 */
@WebServlet("/NouvelleVente")
public class NouvelArticleServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			List<Categorie> listeCategories = CategorieManager.getCategorieManager().allCategorie();
			request.setAttribute("lstCategories", listeCategories);
			request.getRequestDispatcher("/nouvelArticle.jsp").forward(request, response);
		} catch (BusinessException e) {
			e.printStackTrace();
		}

	
}
	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String pseudo = request.getParameter("pseudo");
		String motDePasse = request.getParameter("motDePasse");
		
		

		// Appelle BLL
		try {
			Utilisateur u = UtilisateurManager.getUtilisateurManager().validateConnexion(pseudo, motDePasse);
			HttpSession session = request.getSession();
			session.setAttribute("userInSession", u);
			request.setAttribute("user", u);
			System.out.println("utilisateur connecté");
			request.getRequestDispatcher("/WEB-INF/nouvelArticle.jsp").forward(request, response);
		} catch (BusinessException e) {
			request.setAttribute("errors", e.getErrors());
			request.getRequestDispatcher("/WEB-INF/welcomeLogin.jsp").forward(request, response);
		}

		List<String> errors = new ArrayList<>();
		;
		// date début d'enchère
		LocalDate dateDebutEnchere = LocalDate.parse(request.getParameter("datedebutEnchere"));
		LocalDate dateFinEnchere = LocalDate.parse(request.getParameter("dateFinEnchere"));
		if (dateDebutEnchere == null || dateDebutEnchere.compareTo(dateFinEnchere) < 0 || dateFinEnchere==null)
			errors.add("La date est incorrecte");

	// nomArticle
	String nomArticle = request.getParameter("nomArticle");
	if(nomArticle==null||nomArticle.trim().isEmpty())
	{
		// Erreur de surface
		errors.add("Le nom de l'article doit être renseigné");
	}
	// description
	String description = request.getParameter("description");
	if(description==null||description.trim().isEmpty())
	{
		// Erreur de surface
		errors.add("La description doit être renseignée");
	}
	// categorie
	int noCategorie = Integer.parseInt(request.getParameter("noCategorie"));
	if(noCategorie== 0)
	{
		// Erreur de surface
		errors.add("Veuillez choisir une catégorie");
	}
	// Mise à prix
	int miseAPrix = Integer.parseInt(request.getParameter("miseAPrix"));
	if(miseAPrix==0)
	{
		// Erreur de surface
		errors.add("Veuillez indiquer un prix de départ");
	}

	// Pas d'erreur --> Appel BLL
	if(errors.size()==0)
	{
		try {
			ArticleManager.addArticle(nomArticle, description, noCategorie, miseAPrix, dateDebutEnchere, dateFinEnchere);

			response.sendRedirect(request.getContextPath() + "/encheres");
		} catch (BusinessException e) {
			e.printStackTrace();
			request.setAttribute("nomArticle", nomArticle);
			request.setAttribute("description", description);
			request.setAttribute("noCategorie", noCategorie);
			request.setAttribute("miseAPrix", miseAPrix);
			request.setAttribute("dateDebutEnchere", dateDebutEnchere);
			request.setAttribute("dateFinEnchere", dateFinEnchere);
			request.setAttribute("errors", e.getErrors());
			RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/nouvelArticle.jsp");
			rd.forward(request, response);
		}
	}else
	{
		// Erreur --> retourne sur la JSP
		request.setAttribute("nomArticle", nomArticle);
		request.setAttribute("description", description);
		request.setAttribute("noCategorie", noCategorie);
		request.setAttribute("miseAPrix", miseAPrix);
		request.setAttribute("dateDebutEnchere", dateDebutEnchere);
		request.setAttribute("dateFinEnchere", dateFinEnchere);
		request.setAttribute("errors", errors);
		RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/nouvelArticle.jsp");
		rd.forward(request, response);
	}

}}


