/**
 * 
 */
package fr.eni.javaee.trocenchere.bo;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

/**
 * @author acueff2020
 *
 */
public class Article {
	private int noArticle;
	private String nomArticle;
	private String description;
	private Date dateDebutEncheres;
	private Date dateFinEncheres;
	private int miseAPrix;
	private int prixvente;
	private boolean etatVente;
	private Categorie categorie;
	private int noCategorie;
	private int noUtilisateur;
	private Retrait retrait;
	List<Enchere> encheres = new ArrayList<Enchere>();
	
	
	public Article() {
		super();
	}
	public Article(String nomArticle, String description, Date dateDebutEncheres, Date dateFinEncheres, int noCategorie,
			int noUtilisateur) {
		super();
		this.nomArticle = nomArticle;
		this.description = description;
		this.dateDebutEncheres = dateDebutEncheres;
		this.dateFinEncheres = dateFinEncheres;
		this.noCategorie = noCategorie;
		this.noUtilisateur = noUtilisateur;
	}
	
	
	public Article(int noArticle, String nomArticle, String description, Date dateDebutEncheres,
			Date dateFinEncheres, int miseAPrix, int prixvente, boolean etatVente, Categorie categorie, Retrait retrait,
			List<Enchere> encheres) {
		super();
		this.noArticle = noArticle;
		this.miseAPrix = miseAPrix;
		this.prixvente = prixvente;
		this.etatVente = etatVente;
		this.categorie = categorie;
		this.retrait = retrait;
		this.encheres = encheres;
	}
	
			
	
	public Article(String nomArticle,String description,int noCategorie, int miseAPrix,
			Date dateDebutEnchere, Date dateFinEnchere) {
		super();
	}

	
	public int getNoArticle() {
		return noArticle;
	}
	public void setNoArticle(int noArticle) {
		this.noArticle = noArticle;
	}
	public String getNomArticle() {
		return nomArticle;
	}
	public void setNomArticle(String nomArticle) {
		this.nomArticle = nomArticle;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Date getDateDebutEncheres() {
		return dateDebutEncheres;
	}
	public void setDateDebutEncheres(Date dateDebutEncheres) {
		this.dateDebutEncheres = dateDebutEncheres;
	}
	public Date getDateFinEncheres() {
		return dateFinEncheres;
	}
	public void setDateFinEncheres(Date dateFinEncheres) {
		this.dateFinEncheres = dateFinEncheres;
	}
	public int getMiseAPrix() {
		return miseAPrix;
	}
	public void setMiseAPrix(int miseAPrix) {
		this.miseAPrix = miseAPrix;
	}
	public int getPrixvente() {
		return prixvente;
	}
	public void setPrixvente(int prixvente) {
		this.prixvente = prixvente;
	}
	public boolean isEtatVente() {
		return etatVente;
	}
	public void setEtatVente(boolean etatVente) {
		this.etatVente = etatVente;
	}
	public Categorie getCategorie() {
		return categorie;
	}
	public void setCategorie(Categorie categorie) {
		this.categorie = categorie;
	}
	public Retrait getRetrait() {
		return retrait;
	}
	public void setRetrait(Retrait retrait) {
		this.retrait = retrait;
	}
	public List<Enchere> getEncheres() {
		return encheres;
	}
	public void setEncheres(List<Enchere> encheres) {
		this.encheres = encheres;
	}
	public int getNoCategorie() {
		return noCategorie;
	}
	public void setNoCategorie(int noCategorie) {
		this.noCategorie = noCategorie;
	}
	public int getNoUtilisateur() {
		return noUtilisateur;
	}
	public void setNoUtilisateur(int noUtilisateur) {
		this.noUtilisateur = noUtilisateur;
	}
	
	@Override
	public String toString() {
		return "Article [noArticle=" + noArticle + ", nomArticle=" + nomArticle + ", description=" + description
				+ ", dateDebutEncheres=" + dateDebutEncheres + ", dateFinEncheres=" + dateFinEncheres + ", miseAPrix="
				+ miseAPrix + ", prixvente=" + prixvente + ", etatVente=" + etatVente + ", categorie=" + categorie
				+ ", noCategorie=" + noCategorie + ", noUtilisateur=" + noUtilisateur + ", retrait=" + retrait
				+ ", encheres=" + encheres + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + noArticle;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Article other = (Article) obj;
		if (noArticle != other.noArticle)
			return false;
		return true;
	}
	
	
	

}
