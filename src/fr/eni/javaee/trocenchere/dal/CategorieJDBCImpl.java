/**
 * 
 */
package fr.eni.javaee.trocenchere.dal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import fr.eni.javaee.trocenchere.bo.Categorie;
import fr.eni.javaee.trocenchere.exception.BusinessException;
import fr.eni.javaee.trocenchere.utils.Errors;

/**
 * @author dfrage2020
 *
 */
public class CategorieJDBCImpl implements CategorieDAO {
	private static final String SELECT_CATEGORIE = "SELECT libelle, no_categorie FROM CATEGORIES";

	

	@Override
	public List<Categorie> findAllCategorie() throws BusinessException {
		List<Categorie> lstCategories = new ArrayList<Categorie>();
		try (Connection cnx = ConnectionProvider.getConnection()) {
			PreparedStatement pstmt = cnx.prepareStatement(SELECT_CATEGORIE);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				lstCategories.add(new Categorie(rs.getInt("no_categorie"), rs.getString("libelle")));
			
			}
		} catch (Exception e) {
			e.printStackTrace();
			BusinessException businessException = new BusinessException();
			businessException.addError(Errors.LECTURE_LISTE_CATEGORIE_ECHEC);
			throw businessException;
		}
		return lstCategories;
	}


	
	@Override
	public void create(Categorie categorie) throws BusinessException {
	}

	
}
