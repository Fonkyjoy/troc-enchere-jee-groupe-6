<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@page import="java.util.List"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page errorPage="error.java" isErrorPage="false"%>
<!DOCTYPE html>
<html lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content=" ">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>TrocEnchere</title>

    <!-- Bootstrap -->
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- <link href="${pageContext.servletContext.contextPath}/Vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">-->
</head>

<body style="background-color: rgb(252, 245, 235);">
	<!-- header -->
    <!-- nav -->
    <nav class="navbar navbar-default navbar navbar-inverse ">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand " href="#" style="font-size:20% font-weight:bold ">
                  <h4 style="font-weight:bold"> ENI-Enchères</h4>
                  </a>
            </div>
        </div>
    </nav>
     <div class="container"> 
        <div class="row" >
            <div class="col-sm-12 ">
                <div class="row">
                    <div class="col-xs-4 col-xs-offset-4" style=" height: 100px ;">
                        <h2 style="font-weight:bold">Mon profil</h2> 
                    </div>
                </div>
                <form class="form-horizontal" method="post" action="${pageContext.request.contextPath}/CreerCompte">
                    <!-- rang 1 -->
                    <div class="form-group col-sm-6 ">
                        <label for="pseudo" class="col-sm-6 control-label alignL" >Pseudo : </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control " id="pseudo" placeholder="Pseudo" name="pseudo" required>
                        </div>
                    </div>
                    <div class="form-group col-sm-6 ">
                        <label for="nom" class="col-sm-6 control-label alignL" >Nom : </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="nom" name="nom" placeholder="Nom" required>
                        </div>
                    </div>

                    <!-- rang 2 -->
                    <div class="form-group col-sm-6 ">
                        <label for="prenom" class="col-sm-6 control-label alignL" >Prénom: </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="prenom" name="prenom" placeholder="Prénom" required>
                        </div>
                    </div>
                    <div class="form-group col-sm-6 ">
                        <label for="email" class="col-sm-6 control-label ">Email : </label>
                        <div class="col-sm-6">
                            <input type="email" class="form-control" id="email" name="email" placeholder="email" required>
                        </div>
                    </div>
                    <!-- rang 3 -->
                    <div class="form-group col-sm-6 ">
                        <label for="telephone" class="col-sm-6 control-label alignL">Teléphone : </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="telephone" name="telephone" placeholder="Téléphone" required>
                        </div>
                    </div>
                    <div class="form-group col-sm-6 ">
                        <label for="rue" class="col-sm-6 control-label alignL">Rue : </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="rue" name="rue" placeholder="Rue" required>
                        </div>
                    </div>
                    <!-- rang 4 -->
                    <div class="form-group col-sm-6 ">
                        <label for="postcode" class="col-sm-6 control-label alignL">Code postal : </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="postcode" name="postcode" placeholder="Code Postal" required>
                        </div>
                    </div>
                    <div class="form-group col-sm-6 ">
                        <label for="ville" class="col-sm-6 control-label alignL">Ville : </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="ville" name="ville" placeholder="Ville" required>
                        </div>
                    </div>
                    <!-- rang 5 -->
                    <div class="form-group col-sm-6 ">
                        <label for="password" class="col-sm-6 control-label alignL">Mot de passe : </label>
                        <div class="col-sm-6">
                            <input type="password" class="form-control" id="password" name="password" placeholder="Password" required>
                        </div>
                    </div>
                    <div class="form-group col-sm-6 ">
                        <label for="repassword" class="col-sm-6 control-label alignL">Confirmation : </label>
                        <div class="col-sm-6">
                            <input type="password" class="form-control" id="repassword" name="repassword" placeholder="Confirmer Password" required>
                        </div>
                    </div>
					
                    <div class=" row form-group" >
                        <div class=" col-sm-3 col-sm-offset-4">
							<input type="submit" value="Créer" class="btn btn-primary btn-lg active" href="${pageContext.request.contextPath}/AfficheProfil"> 	
							
						</div>
                        <div class="col-sm-3 ">
                            <a  class="btn btn-primary btn-lg active" href="${pageContext.request.contextPath}/encheres">Annuler</a>
                        </div>
                    </div>

                </form>
			 </div> 

            </div> 
        </div>

        <!-- FOOTER -->

        <script src="https://cdn.jsdelivr.net/npm/jquery@1.12.4/dist/jquery.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js"></script>
    <!-- </div> -->
</body>

</html>