/**
 * 
 */
package fr.eni.javaee.trocenchere.dal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import fr.eni.javaee.trocenchere.bo.Article;
import fr.eni.javaee.trocenchere.bo.Categorie;
import fr.eni.javaee.trocenchere.bo.Utilisateur;
import fr.eni.javaee.trocenchere.exception.BusinessException;

/**
 * @author acueff2020
 *
 */
public class ArticleJDBCImpl implements ArticleDAO {
	private static final String CREATE_ARTICLE ="INSERT INTO ARTICLES_VENDUS (nom_article, description,date_debut_encheres,date_fin_encheres,no_utilisateur,no_categorie)\r\n"
			+ "VALUES (?,?,?,?,?,?)";

	public void createArticle(Article article) throws BusinessException {
		try (Connection cnx = ConnectionProvider.getConnection()){
			PreparedStatement pstmt = cnx.prepareStatement(CREATE_ARTICLE);
		pstmt.setString(1, article.getNomArticle());
		pstmt.setString(2, article.getDescription());
		pstmt.setDate(3, article.getDateDebutEncheres());
		pstmt.setDate(4, article.getDateFinEncheres());
		pstmt.setInt(5, article.getNoUtilisateur());
		pstmt.setInt(6, article.getNoCategorie());
		pstmt.executeUpdate();
		ResultSet rs = pstmt.getGeneratedKeys();
		if(rs.next()) {
			article.setNoArticle(rs.getInt(1));
		}else {
			BusinessException be = new BusinessException();
			be.addError("Veuillez renseigner tous les champs");
			throw be;
		}
		rs.close();
	}catch(SQLException e){
		BusinessException be = new BusinessException();
		be.addError("Impossible de créer un nouvel article en BD");
	}

	
		
	}

	@Override
	public Article selectArticle(int noUtilisateur) throws BusinessException {
		Article article = new Article();
		try(Connection cnx = ConnectionProvider.getConnection()) {
			PreparedStatement pstmt = cnx.prepareStatement(CREATE_ARTICLE);
			pstmt.setInt(1, noUtilisateur);
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				article = articleBuilder(rs);				
			}
		} catch (SQLException e) {
			e.printStackTrace();
			BusinessException be = new BusinessException();
			be.addError("Impossible de trouver pseudo ");
			throw be;
		}
		return article;
		
	}

	/**
	 * @param rs
	 * @return
	 */
	private Article articleBuilder(ResultSet rs) throws SQLException{
		Article article = new Article();
		article.setNoUtilisateur(rs.getInt("no_utilisateur"));
		article.setNomArticle(rs.getString("nom_article"));
		article.setDateDebutEncheres(rs.getDate("dateDebutEncheres"));
		article.setDateFinEncheres(rs.getDate("dateFinEncheres"));
		article.setMiseAPrix(rs.getInt("miseAPrix"));
		article.setNoCategorie( rs.getInt("noCategorie"));
		return article;	
	}
}
