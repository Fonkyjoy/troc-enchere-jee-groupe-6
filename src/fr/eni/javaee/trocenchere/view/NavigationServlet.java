package fr.eni.javaee.trocenchere.view;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import fr.eni.javaee.trocenchere.bll.CategorieManager;
import fr.eni.javaee.trocenchere.bo.Categorie;
import fr.eni.javaee.trocenchere.exception.BusinessException;

/**
 * Servlet implementation class navigation
 */
@WebServlet("/encheres")
public class NavigationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		if(request.getParameter("lstCategories")!=null) {
			try {
				List<Categorie> listeCategories = CategorieManager.getCategorieManager().allCategorie();
				request.setAttribute("lstCategories", listeCategories);
				request.getRequestDispatcher("/WEB-INF/index.jsp").forward(request, response);
			} catch (BusinessException e) {
				e.printStackTrace();
			}

		
	}


}