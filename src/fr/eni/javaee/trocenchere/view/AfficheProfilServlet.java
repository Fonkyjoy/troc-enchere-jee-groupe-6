package fr.eni.javaee.trocenchere.view;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.eni.javaee.trocenchere.bll.UtilisateurManager;
import fr.eni.javaee.trocenchere.bo.Utilisateur;
import fr.eni.javaee.trocenchere.exception.BusinessException;

/**
 * Servlet implementation class AfficheProfilServlet
 */
@WebServlet("/AfficheProfil")
public class AfficheProfilServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//request.getRequestDispatcher("/ModifProfilServlet").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		Utilisateur utilisateur = null;
		String nomUtilisateur = null;
		UtilisateurManager utilisateurManager = new UtilisateurManager();
		
		if (session.getAttribute("nomUtilisateur") != null) {
			utilisateur = (Utilisateur) session.getAttribute("nomUtilisateur");
			nomUtilisateur = utilisateur.getPseudo();
		}
		try {
			
			nomUtilisateur = request.getParameter("pseudo");
			utilisateur = utilisateurManager.afficheUtilisateur(nomUtilisateur);
			request.setAttribute("utilisateur", utilisateur);
			
			request.getRequestDispatcher("/WEB-INF/afficheProfil.jsp").forward(request, response);
		} catch (BusinessException e) {
			e.printStackTrace();
			request.setAttribute("errors", e.getErrors());
		}

	}
}
