<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Navigation</title>

<link href="Vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">


</head>
<body>
	<main class="container" style="border: 1px solid blue">

		<header class="row justify-content-end ">
			<!-- header col1 -->
			<div class="col-lg-12 col-md-12 col-sm-12 col-12 ">
				<h1 class="my-4">ENI-Encheres</h1>

				<nav class="row justify-content-end ">
					<div class="col-lg-2 col-md-2 col-sm-12 col-12 ">
						<a href="">Enchères</a>
					</div>
					<div class="col-lg-2 col-md-2 col-sm-12 col-12 ">
						<a href="">Vendre un article</a>
					</div>
					<div class="col-lg-2 col-md-2 col-sm-12 col-12 ">
						<a href="">Mon profil</a>
					</div>
					<div class="col-lg-2 col-md-2 col-sm-12 col-12 ">
						<a href="">Déconnexion</a>
					</div>

				</nav>
			</div>
			<!-- header col2 -->
		</header>




		<!-- Page raw 2 -->
		<section class="row" style="border: 1px solid green">
			<!-- section Filtres -->
			<div class="col-lg-6 col-md-6 col-sm-12 col-12 ">
				<h3>Filtres</h3>
				<nav class="navbar navbar-light bg-light">
					<form class="form-inline">
						<input class="form-control mr-sm-2" type="search"
							placeholder="Le nom de l'article contient" aria-label="Search">
						<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Rechercher</button>
					</form>
				</nav>

				<aside class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-12">
						<label for="lstcate">Catégorie : </label> <select id="lstcate">
							<option selected value="SELECT">Toutes</option>
							<c:forEach items="${lstCategories}" var="c">
								<option value="${c.libelle}">${c.libelle}</option>
							</c:forEach>
						</select>
					</div>
				</aside>
			</div>

		</section>

		<!--  Page raw3 -->
		<section class="row" style="border: 3px solid orange">
			<!-- section Filtres -->
			<div class="col-lg-9 col-md-6 col-sm-12 col-12 ">

				<div class="form-check form-check-inline">
					<input class="form-check-input" type="radio"
						name="inlineRadioOptions" id="inlineAchat" value="Achat" /> <label
						class="form-check-label" for="inlineAchat">Achats</label>

		
						<input class="form-check-input" type="checkbox" value=""
							id="defaultCheck1"> <label class="form-check-label"
							for="defaultCheck1"> enchères ouvertes </label>
					


				</div>
				<!-- fn div achat -->

				<div class="form-check form-check-inline">
					<input class="form-check-input" type="radio"
						name="inlineRadioOptions" id="inlineVente" value="Ventes" /> <label
						class="form-check-label" for="inlineVente">Mes ventes</label>
				</div>

				<aside class=" justify-content-end" style="border: 3px solid blue">
					<h3>Rechercher</h3>
				</aside>
			</div>

		</section>

		<!-- Page raw4 -->
		<section class="row" style="border: 1px solid pink">
			<!-- section Filtres -->
			<div class="btn-group col-lg-6 col-md-6 col-sm-12 col-12"
				style="border: 1px solid red">
				<div class="card mb-3" style="max-width: 700px;">
					<div class="row g-0">
						<div class="col-lg-5 col-md-5 col-sm-5 col-5">
							<img src="./images/pc.jpg" class="img-thumbnail"
								alt="image de l'article a vendre">
						</div>
						<div class="col-lg-7 col-md-7 col-sm-7 col-7">
							<div class="card-body">
								<h5 class="card-title">PC Gamer pour Travailler</h5>
								<p class="card-text">prix:</p>
								<p class="card-text">Fin de l'enchère:</p>
								<p class="card-text">Vendeur:</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- fin conteneur -->
	</main>
	<footer> </footer>
</body>
</html>