/**
 * 
 */
package fr.eni.javaee.trocenchere.bll;

import java.util.List;

import fr.eni.javaee.trocenchere.bo.Categorie;
import fr.eni.javaee.trocenchere.dal.CategorieDAO;
import fr.eni.javaee.trocenchere.dal.DAOFactory;
import fr.eni.javaee.trocenchere.exception.BusinessException;

/**
 * @author dfrage2020
 *
 */
public class CategorieManager {
	private CategorieDAO categorieDAO;
	/**
	 * Singleton
	 */
private static CategorieManager instance;
	
	public static CategorieManager getCategorieManager() {
		if(instance == null) {
			instance = new CategorieManager();
		}
		return instance;
	}
	
	public CategorieManager() {
		categorieDAO = DAOFactory.getCategorieDAO();
	}
	
	//Appel a la couche DAL
	public List<Categorie> allCategorie() throws BusinessException{
		return categorieDAO.findAllCategorie();
	}
	
}
