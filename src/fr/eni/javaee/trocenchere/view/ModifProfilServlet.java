package fr.eni.javaee.trocenchere.view;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.eni.javaee.trocenchere.bll.UtilisateurManager;
import fr.eni.javaee.trocenchere.bo.Utilisateur;
import fr.eni.javaee.trocenchere.exception.BusinessException;

/**
 * Servlet implementation class ModifProfilServlet
 */
@WebServlet("/ModifProfilServlet")
public class ModifProfilServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("je suis modifServlet L26");
		//UtilisateurManager utilisateurManager = new UtilisateurManager();
		HttpSession session = request.getSession();
		Utilisateur userInSession = (Utilisateur) session.getAttribute("userInSession");
		UtilisateurManager utilisateurManager = UtilisateurManager.getUtilisateurManager();
		try {
			Utilisateur userCompletUtilisateur=utilisateurManager.afficheUtilisateur(userInSession.getPseudo());
			session.setAttribute("userInSession", userCompletUtilisateur);
		} catch (BusinessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		/*System.out.println("je suis modifServlet L31");
		session.setAttribute("utilisateur", utilisateur);*/
		
		System.out.println("je suis modifServlet L33");
		request.getRequestDispatcher("/WEB-INF/modifProfil.jsp").forward(request, response);	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("je suis modifServlet do post L43");
		request.setCharacterEncoding("UTF-8");
		UtilisateurManager utilisateurManager = new UtilisateurManager();
		Utilisateur utilisateur = new Utilisateur();
		try {
			
			utilisateur.setNom(request.getParameter("nom"));
			utilisateur.setPrenom(request.getParameter("prenom"));
			utilisateur.setEmail(request.getParameter("email"));
			utilisateur.setTelephone(request.getParameter("telephone"));
			utilisateur.setRue(request.getParameter("rue"));
			utilisateur.setCodePostal(request.getParameter("ville"));
			utilisateur.setVille(request.getParameter("ville"));
			utilisateur.setMotDePasse(request.getParameter("password"));
			String newpassword = request.getParameter("newpassword");
			String renewpassword = request.getParameter("renewpassword");
			System.out.println("je suis modifServlet do post L65");
			//if(newpassword == renewpassword) {
				utilisateurManager.modifUtilisateur(utilisateur);
				System.out.println("je suis modifServlet do post L68");
			//}
				
			//TODO session utilisateur
			
			HttpSession session =  request.getSession();
			session.setMaxInactiveInterval(60*60); 
			session.setAttribute("userconnecte", utilisateur);
			System.out.println("je suis modifServlet do post L76");
			request.getRequestDispatcher("/WEB-INF/modifProfil.jsp").forward(request, response);

		} catch (BusinessException e) {
			e.printStackTrace();
			request.setAttribute("errors", e.getErrors());
			request.getRequestDispatcher("/WEB-INF/modifProfil.jsp").forward(request, response);
		}	
	}
	protected void doDelete(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {
		
	}

}
